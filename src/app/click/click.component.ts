import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-click',
  templateUrl: './click.component.html',
  styleUrls: ['./click.component.scss']
})
export class ClickComponent implements OnInit {

  // @Input() clickC1 !: number
  // @Input() clickC2 !: number

  countClickC1 = 0;
  countClickC2 = 0;
  onclickedC1(){
    this.countClickC1 +=1;
  }

  onclickedC2(){
    this.countClickC2 +=1;
  }


  onC1Click(){

  }

  constructor() { }

  ngOnInit(): void {
  }

}
