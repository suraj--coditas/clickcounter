import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'click-count-Application';


  countClickC1 = 0;
  countClickC2 = 0;

  onclickedC1(){
    this.countClickC1 +=1;
  }

  onclickedC2(){
    this.countClickC2 +=1;
  }
}
